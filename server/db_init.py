from random import choice, shuffle
from time import time

from config import engine
from faker import Faker
from models import Chat, Message
from sqlmodel import Session, SQLModel


def database_init():
    fake = Faker()
    chat_types = ["user", "group"]
    SQLModel.metadata.create_all(engine)
    with Session(engine) as session:
        from_ids = [x + 1 for x in range(100)]
        to_ids = [x + 1 for x in range(100)]
        for chat_id in from_ids:
            session.add(
                Chat(id=chat_id, type=choice(chat_types), name=fake.name())
            )
        shuffle(from_ids)
        shuffle(to_ids)
        while from_ids:
            from_id = from_ids.pop()
            to_id = to_ids.pop()
            session.add(
                Message(
                    from_id=from_id,
                    to_id=to_id,
                    content=fake.text(),
                    timestamp=time(),
                )
            )
        session.commit()


if __name__ == "__main__":
    database_init()
