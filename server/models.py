from typing import Optional

from config import engine
from sqlmodel import Field, Session, SQLModel, select


class Chat(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    type: str
    name: str


class Message(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    from_id: int = Field(foreign_key="chat.id")
    to_id: int = Field(foreign_key="chat.id")
    content: str
    timestamp: float
    theme_id: Optional[int] = Field(default=None)


if __name__ == "__main__":
    with Session(engine) as session:
        # query = select(Message)
        # messages = session.exec(query)
        # for message in messages:
        #     print(message)
        query = select(Chat).where(Chat.type == "group")
        chats = session.exec(query).all()
        print(chats)
