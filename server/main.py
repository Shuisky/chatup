import asyncio

from websockets.server import serve


async def handler(websocket):
    name = await websocket.recv()
    print(f"<<< {name}")

    greeting = f"Hello, {name}!"

    await websocket.send(greeting)
    print(f">>> {greeting}")


async def main():
    async with serve(handler, "localhost", 8765):
        await asyncio.Future()


if __name__ == "__main__":
    asyncio.run(main())
