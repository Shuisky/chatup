import asyncio

from websockets.client import connect


async def hello():
    async with connect("ws://localhost:8765") as websocket:
        await websocket.send("Hello world!")
        await websocket.recv()


asyncio.run(hello())
